
package tareadosagendacontactos;

import java.io.Serializable;
import java.util.Date;



public class ListContacto implements Serializable{
    private NodoContacto cabeza = null;
    private int longitud = 0;
    
    public ListContacto(){
        this.cabeza = null;
    }

    public boolean estaVacia(){
        return cabeza == null; 
    }
    
    public void insertarPrincipio(Contacto contacto){
        NodoContacto nodo = new NodoContacto(contacto);
        nodo.siguiente = cabeza; 
        cabeza = nodo;
        this.longitud++; 
    }

    public Contacto buscarContacto(Contacto contacto){ //Si no encuentra nada regresa null busca contactos por objeto
        NodoContacto puntero = cabeza;
        while (puntero != null) {
            if (contacto.equals(puntero.contacto)) {
                return puntero.contacto;
            }
            puntero = puntero.siguiente;
        }
        return null;
    }
    
    public Contacto buscarContactoTelefono(long telefono){
        NodoContacto puntero = cabeza;
        while (puntero != null) {
            if (telefono == puntero.contacto.getTelefono()) {
                return puntero.contacto;
            }
            puntero = puntero.siguiente;
        }
        return null;
    }
     
    
    public Contacto buscarContactoEmail(String email){
        NodoContacto puntero = cabeza;
        while (puntero != null) {
            if (email.equals(puntero.contacto.getEmail().toLowerCase())) {
                return puntero.contacto;
            }
            puntero = puntero.siguiente;
        }
        return null;
    }
    
    
    public void borrarContactoNumero(long telefono){
        if (cabeza != null) {
            if (telefono == cabeza.contacto.getTelefono()) {
                System.out.println("El contacto: \n" + cabeza.contacto + "\nSE ELIMINÓ. ");
                cabeza = cabeza.siguiente;   
            }else{
                NodoContacto pred = cabeza, tmp = cabeza.siguiente;  //Pred anterior y tmp el siguiente a pred
                while (tmp != null) {
                /*  System.out.println("Pred: " + pred);
                    System.out.println("pred.siguiente" + pred.siguiente);
                    System.out.println("tmp: " + tmp);
                    System.out.println("tmp.siguiente" + tmp.siguiente);
                */
                    if (tmp.contacto.getTelefono() == telefono) {
                        System.out.println("El contacto: \n" + tmp.contacto + "\nSE ELIMINÓ. ");
                        pred.siguiente = tmp.siguiente;
                    }
                    pred = pred.siguiente;
                    tmp = tmp.siguiente;
                    
                }
            }
        this.longitud--;    
        }else{
            System.out.println("No hay nada en su agenda.");
        }
    }
    
    public void borrarContactoEmail(String email){
        if (cabeza != null) {
            if (email.equals(cabeza.contacto.getEmail())) {
                System.out.println("El contacto: \n" + cabeza.contacto + "\nSE ELIMINÓ. ");
                cabeza = cabeza.siguiente;
            }else{
                NodoContacto pred = cabeza, tmp = cabeza.siguiente;  //Pred anterior y tmp el siguiente a pred
                while (tmp != null) {
                /*  System.out.println("Pred: " + pred);
                    System.out.println("pred.siguiente" + pred.siguiente);
                    System.out.println("tmp: " + tmp);
                    System.out.println("tmp.siguiente" + tmp.siguiente);
                */
                    if (tmp.contacto.getEmail().equals(email)) {
                        System.out.println("El contacto: \n" + tmp.contacto + "\nSE ELIMINÓ. ");
                        pred.siguiente = tmp.siguiente;
                    }
                    pred = pred.siguiente;
                    tmp = tmp.siguiente;
                    
                }
            }
            this.longitud--;
        }else{
            System.out.println("No hay nada en su agenda.");
        }
    }
    
    public void modificarContacto(String nombre, Date fdn, String email, long telefono){   //MODIFICAR
        NodoContacto puntero = cabeza;
        while (puntero != null) {
            if (puntero.contacto.getEmail().equals(email) && puntero.contacto.getTelefono() == telefono) {
                System.out.println("Contacto a modificar: " + puntero.contacto);
                puntero.contacto.setNombre(nombre);
                puntero.contacto.setFdn(fdn);
                System.out.println(buscarContacto(puntero.contacto));
                break;
            }else if (puntero.contacto.getEmail().equals(email) && puntero.contacto.getTelefono() != telefono) {
                puntero.contacto.setNombre(nombre);
                puntero.contacto.setFdn(fdn);
                puntero.contacto.setTelefono(telefono);
                System.out.println(buscarContacto(puntero.contacto));
                break;
            }else if(puntero.contacto.getTelefono() == telefono && !puntero.contacto.getEmail().equals(email)){
                puntero.contacto.setNombre(nombre);
                puntero.contacto.setFdn(fdn);
                puntero.contacto.setEmail(email);
                System.out.println(buscarContacto(puntero.contacto));
                break;
            }
        }
        System.out.println("El contactó no se encontró, por lo tanto no se modificaron sus datos, para que el contacto sea "
                + "modificado, debe tener su número de teléfono o su correo registrados en la agenda");
    }
     
    public void printAll(){
        for (NodoContacto tmp = cabeza;  tmp != null; tmp = tmp.siguiente) {
            System.out.println(tmp.contacto + " " );
        }
    }
    
    
    public int contar(){
        return this.longitud;
    }
    
}
