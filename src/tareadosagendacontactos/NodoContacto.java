
package tareadosagendacontactos;

import java.io.Serializable;

public class NodoContacto implements Serializable{
    public Contacto contacto;
    public NodoContacto siguiente = null;
    
    public NodoContacto(Contacto contacto){
        this.contacto = contacto;
    }
}
