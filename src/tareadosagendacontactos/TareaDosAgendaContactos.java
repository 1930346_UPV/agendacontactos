
package tareadosagendacontactos;

import java.util.Date;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.FileInputStream;
import java.util.Scanner;
import java.util.StringTokenizer;

public class TareaDosAgendaContactos {

    public static void main(String[] args) {
    
        String nombre;
        Date fdn;
        String fechaCompleta;
        String email;
        long telefono;
        String eleccion = "";
        String opcion = "";
        
        Contacto contacto = null;
        ListContacto agenda = new ListContacto();
        Scanner entrada = new Scanner(System.in);
        FileOutputStream fos = null;
        ObjectOutputStream salida = null;
        StringTokenizer st = null;
        FileInputStream fis = null;
        ObjectInputStream archivoEntrada = null;
        
        
        try {
            fis = new FileInputStream("agenda.ser");
            archivoEntrada = new ObjectInputStream(fis);
            
            agenda = (ListContacto) archivoEntrada.readObject(); //Se pone conectacto porque sabemos
                        //Pero en caso de que no sepamos se utiliza un getClass();
            //System.out.println("CONTACTO: " + contacto.toString());
        } catch (Exception e) {
            //e.printStackTrace();
        }
        /*finally{
            try {
                if (archivoEntrada != null) {
                    entrada.close();
                }
                
                if (fis != null) {
                    fis.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        */
        try {
            
            do {
                System.out.println();
                System.out.println("¿Qué desea realizar?");
                System.out.println("1.- Crear contacto");
                System.out.println("2.- Borrar Contacto");
                System.out.println("3.- Buscar contacto");
                System.out.println("4.- Editar contacto");
                System.out.println("5.- Visualizar contactos");
                System.out.print("Respuesta: ");
                opcion = entrada.nextLine();

                switch (opcion) {
                    case "1":
                        System.out.println("\nIngrese por favor los datos del contacto: ");
                        System.out.print("Nombre: " );
                        nombre = entrada.nextLine();
                        System.out.print("Fecha de nacimiento (yyyy mm dd): ");
                        fechaCompleta = entrada.nextLine();
                        st = new StringTokenizer(fechaCompleta, " ");
                        fdn = new Date((Integer.parseInt(st.nextElement().toString())-1900), (Integer.parseInt(st.nextElement().toString())-1), Integer.parseInt(st.nextElement().toString()));
                        System.out.print("email: ");
                        email = entrada.nextLine();
                        System.out.print("Telefono: ");
                        telefono = entrada.nextLong();
                        entrada.nextLine();    //Se agrega esto, porque nextLong no consume el \n, entonces no funcionaría el ciclo
                        contacto = new Contacto(nombre, fdn, email, telefono);
                        agenda.insertarPrincipio(contacto);
                        System.out.println("\nEl contacto se añadió. ");
                        System.out.println("-------- Agenda --- Contactos: " +  agenda.contar() + " --------");
                        agenda.printAll();
                        System.out.println();

                        break;
                    case "2":
                        System.out.println("\n1.- Borrar por su teléfono.");
                        System.out.println("2.- Borrar por su email.");
                        System.out.print("Elija una opción: ");
                        opcion = entrada.nextLine();
                        switch (opcion) {
                            case "1":
                                System.out.print("Telefono: ");
                                telefono = entrada.nextLong();
                                entrada.nextLine();    //Se agrega esto, porque nextLong no consume el \n, entonces no funcionaría el ciclo
                                agenda.borrarContactoNumero(telefono);
                                break;
                            case "2":
                                System.out.println("Ingrese el email del contacto: ");
                                email = entrada.nextLine().toLowerCase();
                                agenda.borrarContactoEmail(email);
                                break;
                            default:
                                System.out.println("Opción no existente");
                        }

                        System.out.println("-------- Agenda --- Contactos: " +  agenda.contar() + " --------");
                        agenda.printAll();
                        System.out.println();
                        break;

                    case "3":
                        System.out.println("1.- Buscarlo por teléfono");
                        System.out.println("2.- Buscarlo por email");
                        System.out.println("3.- Buscarlo por sus datos completos");
                        System.out.print("Elija una opción: ");
                        opcion = entrada.nextLine();
                        switch (opcion) {
                            case "1":
                                System.out.print("Telefono: ");
                                telefono = entrada.nextLong();
                                entrada.nextLine();    //Se agrega esto, porque nextLong no consume el \n, entonces no funcionaría el ciclo
                                if (agenda.buscarContactoTelefono(telefono) == null) {
                                    System.out.println("No se encontró al contacto.");
                                }else{
                                    System.out.println("Contactó encontrado: " + agenda.buscarContactoTelefono(telefono).toString());
                                }
                                break;
                            case "2":
                                System.out.println("Ingrese el email del contacto: ");
                                email = entrada.nextLine().toLowerCase();
                                if (agenda.buscarContactoEmail(email) == null) {
                                    System.out.println("No se encontró al contacto.");
                                }else{
                                    System.out.println("Contactó encontrado: " + agenda.buscarContactoEmail(email).toString());
                                }
                                break;
                            case "3":
                                System.out.println("Ingrese por favor los datos del contacto: ");
                                System.out.print("Nombre: " );
                                nombre = entrada.nextLine();
                                System.out.print("Fecha de nacimiento (yyyy mm dd): ");
                                fechaCompleta = entrada.nextLine();
                                st = new StringTokenizer(fechaCompleta, " ");
                                fdn = new Date((Integer.parseInt(st.nextElement().toString())-1900), (Integer.parseInt(st.nextElement().toString())-1), Integer.parseInt(st.nextElement().toString()));
                                System.out.print("email: ");
                                email = entrada.nextLine();
                                System.out.print("Telefono: ");
                                telefono = entrada.nextLong();
                                entrada.nextLine();    //Se agrega esto, porque nextLong no consume el \n, entonces no funcionaría el ciclo
                                contacto = new Contacto(nombre, fdn, email, telefono);
                                if (agenda.buscarContacto(contacto) == null) {
                                    System.out.println("No se encontró el contacto");
                                }else{
                                    System.out.println("Contacto encontrado: " + agenda.buscarContacto(contacto).toString());
                                }
                                break;                     
                            default:
                                System.out.println("Opción no existente");

                                break;
                        }
                        break;
                    case "4":    
                        System.out.println("Ingrese por favor los datos completos a editar: ");
                        System.out.print("Nombre: " );
                        nombre = entrada.nextLine();
                        System.out.print("Fecha de nacimiento (yyyy mm dd): ");
                        fechaCompleta = entrada.nextLine();
                        st = new StringTokenizer(fechaCompleta, " ");
                        fdn = new Date((Integer.parseInt(st.nextElement().toString())-1900), (Integer.parseInt(st.nextElement().toString())-1), Integer.parseInt(st.nextElement().toString()));
                        System.out.print("email: ");
                        email = entrada.nextLine();
                        System.out.print("Telefono: ");
                        telefono = entrada.nextLong();
                        entrada.nextLine();    //Se agrega esto, porque nextLong no consume el \n, entonces no funcionaría el ciclo
                        agenda.modificarContacto(nombre, fdn, email, telefono);

                        break;
                    case "5":
                        System.out.println("\n");
                        System.out.println("-------- Agenda --- Contactos: " +  agenda.contar() + " --------");
                        System.out.println("Nombre  -  Fecha de nacimiento  -  Email  -  Telefono");
                        agenda.printAll();
                        System.out.println("\n----------------------------------------------------------");
                    default:
                        System.out.println("Opción no existente.");
                        break;

                }
                System.out.print("¿Desea salir(S/N)? ");
                eleccion = entrada.nextLine();
            } while (eleccion.equals("N") || eleccion.equals("n"));
            
            fos = new FileOutputStream("agenda.ser");
            salida = new ObjectOutputStream(fos);
            salida.writeObject(agenda);
        } catch (Exception e) {
            e.printStackTrace();
        }finally{   
            try{
                if (salida != null) {
                    salida.close();
                }
                if (fos != null) {
                    fos.close();
                }
            }catch(IOException e){
                e.printStackTrace();
            }
        }
        
        
        
        
    }
    
}
